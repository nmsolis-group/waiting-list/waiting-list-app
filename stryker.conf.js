/**
* @type {import('@stryker-mutator/api/core').StrykerOptions}
*/

module.exports = {
  comment: "This config was generated using a preset. Please see the handbook for more information: https://github.com/stryker-mutator/stryker-handbook/blob/master/stryker/guides/react.md#react",
  tempDirName: "stryker-tmp",  
  
  mutate: [
    "src/**/*.ts?(x)",
    "!src/**/*@(.test|.spec|Spec).ts?(x)",
    "!src/serviceWorker.ts?(x)",
    "!src/index.ts?(x)",
    "!src/setupTests.ts?(x)",
    "!src/__data__/**"
  ],
  mutator: "typescript",
  testRunner: "jest",
  reporters: [
    "progress",
    "clear-text",
    "html"
  ],
  coverageAnalysis: "off",
  jest: {
    projectType: "create-react-app"
  }
};