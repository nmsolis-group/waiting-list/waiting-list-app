import { ViewMode } from '../../components/viewMode';

test('Values are correct', () => {
    expect(ViewMode.valueOf()).toStrictEqual( 
        {
            CREATE: "create", 
            EDIT: "edit", 
            VIEW: "view"
        });
})