import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { shallow } from 'enzyme';
import { createWaitForElement } from 'enzyme-wait';
import React from 'react';
import CategoryContainer from '../../components/categoryContainer';
import { CategoryList } from '../../components/categoryList';
import { categoryResponseData } from '../../__data__/category.data';

const axiosAdapter = new MockAdapter(axios);

test('Category List renders without errors', () => {
    axiosAdapter.onGet().reply(200, categoryResponseData);
    const categoryList = shallow(<CategoryList />);

    const waitForCategory = createWaitForElement('CategoryContainer');

    return waitForCategory(categoryList)
    .then(component => {
        expect(component.find(CategoryContainer)).toHaveLength(categoryResponseData.data.length);
    })
})
