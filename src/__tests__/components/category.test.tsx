import React from 'react';
import {render} from '@testing-library/react';
import { mount } from 'enzyme';
import {Category, CategoryData} from '../../components/category';
import { ViewMode } from '../../components/viewMode';
import { TextField } from '@material-ui/core';

const categoryData: CategoryData = {
    id: '213',
    attributes: {
        name: 'Test Name',
        description: 'Test Description'
    },
    links: {
        self: '/waiting-list/213'
    },
    relationships: {
        services: {
            links: {
                related: '/waiting-list/213/services'
            }
        }
    }
};

describe('View mode is displayed correctly', () => {

    test('Category fields are being displayed', () => {
        const dom = render(<Category category={categoryData} viewMode={ViewMode.VIEW} />);
        expect(dom.container.textContent).toBe(`${categoryData.attributes.name} - ${categoryData.attributes.description}`);
    })
})

describe('Edit mode is displayed correctly', () => {
    test('Fields are shown in edit mode', () => {
        const element = mount(<Category category={categoryData} viewMode={ViewMode.EDIT} />);

        expect(element.find({'data-id': 'txtName'}).at(0).props().defaultValue).toBe(categoryData.attributes.name);
        expect(element.find({'data-id': 'txtDescription'}).at(0).props().defaultValue).toBe(categoryData.attributes.description);
    })
})

describe('Error message displays when incorrect values are passed', () => {
    test('Correct error message is displayed', () => {
        const dom = render(<Category category={categoryData} viewMode={ViewMode.CREATE} />);
        expect(dom.container.textContent).toBe('An error has occurred.');
    })
})