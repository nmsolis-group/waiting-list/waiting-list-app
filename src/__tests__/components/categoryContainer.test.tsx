import React from 'react';
import { shallow } from 'enzyme';
import CategoryContainer from '../../components/categoryContainer';
import { categoryResponseData } from '../../__data__/category.data';
import { ViewMode } from '../../components/viewMode';
import { Category } from '../../components/category';
import { Button } from '@material-ui/core';

describe('category container displays view mode correctly', () => {

    test('defaults to View mode', () => {
        const element = shallow(<CategoryContainer category={categoryResponseData[0]} />);

        expect(element.find(Category).props().category).toStrictEqual(categoryResponseData[0]);
        expect(element.find(Category).props().viewMode).toBe(ViewMode.VIEW);
    })

    test('shows View mode when specified', () => {
        const element = shallow(<CategoryContainer category={categoryResponseData[0]} initialViewMode={ViewMode.VIEW} />);

        expect(element.find(Category).props().category).toStrictEqual(categoryResponseData[0]);
        expect(element.find(Category).props().viewMode).toBe(ViewMode.VIEW);
    })

    test('switches to Edit mode', () => {
        const element = shallow(<CategoryContainer category={categoryResponseData[0]} initialViewMode={ViewMode.VIEW} />);

        element.find(Button).simulate('click');
        expect(element.find(Category).props().viewMode).toBe(ViewMode.EDIT);
    })
})

describe('shows Edit mode when specified', () => {
    test('show Edit mode', () => {
        const element = shallow(<CategoryContainer category={categoryResponseData[0]} initialViewMode={ViewMode.EDIT} />);

        expect(element.find(Category).props().category).toStrictEqual(categoryResponseData[0]);
        expect(element.find(Category).props().viewMode).toBe(ViewMode.EDIT);
    })

    test('switches to View mode', () => {
        const element = shallow(<CategoryContainer category={categoryResponseData[0]} initialViewMode={ViewMode.EDIT} />);

        element.find(Button).simulate('click');
        expect(element.find(Category).props().viewMode).toBe(ViewMode.VIEW);
    })
})