import React from 'react';
import {render} from '@testing-library/react';
import App from '../App';

describe('Application renders correctly', () => {
  test('renders learn react link', () => {
    const dom = render(<App />);
    
    expect(dom.container.querySelector("[data-id='categoryList']")).toBeTruthy();
  });
  
})
