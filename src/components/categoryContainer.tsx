import React, {Component} from 'react';
import { ViewMode } from './viewMode';
import { Category, CategoryData} from './category';
import { Button } from '@material-ui/core';

const defaultViewMode = ViewMode.VIEW;

export interface CategoryContainerProps { category: CategoryData; initialViewMode?: ViewMode }
interface State { viewMode: ViewMode }

export default class CategoryContainer extends Component<CategoryContainerProps, State> {
    constructor(props: CategoryContainerProps) {
        super(props);

        this.state = {
            viewMode: props.initialViewMode ? props.initialViewMode : defaultViewMode
        }

        this.handleEditClick = this.handleEditClick.bind(this);
    }

    handleEditClick(): void {
        this.setState({
            viewMode: this.state.viewMode === ViewMode.VIEW ? ViewMode.EDIT : ViewMode.VIEW
        });
    }

    render(): React.ReactElement {
        return (
            <div>
                <Button onClick={this.handleEditClick} >Edit</Button>
                <Category viewMode={this.state.viewMode} category={this.props.category} />
            </div>
        );
    }
}