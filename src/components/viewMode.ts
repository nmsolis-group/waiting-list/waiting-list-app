export enum ViewMode {
    CREATE = 'create',
    EDIT = 'edit',
    VIEW = 'view'
}