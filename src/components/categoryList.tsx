import React, { Component } from 'react';
import { CategoryData } from './category';
import CategoryContainer from './categoryContainer';
import { ViewMode } from './viewMode'
import axios from 'axios';

interface State {categories: Array<CategoryData> }

export class CategoryList extends Component<{}, State> {
    constructor(props: {}) {
        super(props);

        this.state = {
            categories: []
        }
    }

    componentDidMount(): void {
        axios.get('http://localhost:8080/waiting-list/api/v1/categories')
            .then(result => {
                this.setState({
                    categories: result.data.data
                })
            });
    }

    render(): React.ReactElement {
        return (
            <div data-id="categoryList">
                {
                    this.state.categories.map((category: CategoryData) => {
                        return <CategoryContainer  
                            key={category.id} 
                            data-testid={category.id} 
                            data-type="category" 
                            category={category} 
                            initialViewMode={ViewMode.VIEW} />
                    })
                }
            </div>

        );
    }
}