import React, { Component } from 'react';
import { ViewMode } from './viewMode'
import { TextField, ButtonGroup, Button } from '@material-ui/core';

interface CategoryProps { category?: CategoryData; viewMode: ViewMode }
export interface CategoryData { 
    id: string;
    attributes: {
        name: string;
        description: string;
    };
    links: {
        self: string;
    };
    relationships: {
        services: {
            links: {
                related: string;
            };
        };
    };
}

export class Category extends Component<CategoryProps> {

    constructor(props: CategoryProps) {
        super(props);

        this.state = {
            category: props.category,
            mode: props.viewMode
        }
    }

    render(): React.ReactElement {

        return (
            <div>
                <CategoryView category={this.props.category} viewMode={this.props.viewMode} />
            </div>
        )
    }
}

function CategoryView(props: CategoryProps ): React.ReactElement {
    if (props.category && props.viewMode === ViewMode.VIEW) {
        return <ViewCategory category={props.category} />
    } else if (props.category && props.viewMode === ViewMode.EDIT) {
        return <EditCategory category={props.category} />
    }

    return <div>An error has occurred.</div>
}

function ViewCategory(props: {category: CategoryData}): React.ReactElement {
    return <div>{props.category.attributes.name} - {props.category.attributes.description}</div>;
}

function EditCategory(props: {category: CategoryData}): React.ReactElement {
    
    return (
        <div>
            <form>
                <div>
                    <TextField id="txtName" data-id="txtName" label="Name" defaultValue={props.category.attributes.name} required />
                </div>
                <div style={{marginTop: '15px'}}>
                    <TextField id="txtDescription" data-id="txtDescription" label="Description" defaultValue={props.category.attributes.description} multiline rows="4" variant="outlined" />
                </div>
                <div style={{alignContent: 'center'}}>
                    <ButtonGroup color="primary" aria-label="outlined primary button group">
                        <Button color="secondary">Cancel</Button>
                        <Button color="primary">Save</Button>
                    </ButtonGroup>
                </div>
            </form>
        </div>
    );
}