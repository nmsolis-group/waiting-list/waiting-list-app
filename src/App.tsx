import React from 'react';
import './App.css';
import { CategoryList } from './components/categoryList';

function App(): React.ReactElement {
  return (
    <CategoryList />
  );
}

export default App;
